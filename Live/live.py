# _*_ coding: UTF-8 _*_
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from Lib import get_report


class LiveAutoReport:
    """ 真人自動報表 """

    def __init__(self, driver, get_img, window_data):
        self.driver = driver
        self.get_img = get_img
        self.window_data = window_data

    def auto_report(self, now=True):
        """ 自動報表 """

        # init
        result_list = []

        if not self.window_data['live_window']:
            # 點擊真人類別->進入RG真人
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='真人']"))).click()
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='reg_live rg']//a[@class='lnk_enterGame']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            self.window_data['live_window'] = self.driver.current_window_handle
        else:
            self.driver.switch_to.window(self.window_data['live_window'])

        # 取得報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report(search_now=now)
        if not member_report:
            result_list.append("False")

        # 確認結果
        check_result = get_member_report.check_report_data(member_report)
        if not check_result:
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
