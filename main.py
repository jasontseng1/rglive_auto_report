# _*_ coding: UTF-8 _*_
import sys
import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from Live import live

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager(driver_version='125.0.6422.141').install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()
driver.get(test_data['url'])

# 視窗控制
window_dict = {
    'member_window': driver.current_window_handle,
    'live_window': None,
}


class LiveLobbyReport(unittest.TestCase):
    """ RG真人大廳報表 """

    live_ar = live.LiveAutoReport(driver, get_img, window_dict)

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_login(self):
        """ 登入 """

        result = True

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result = False
                self.__class__.test_continue = False

            get_img("登入成功")

        except:
            get_img("登入失敗")
            result = False
            self.__class__.test_continue = False

        self.assertEqual(True, result)

    def test_rg_live_lobby_report_now(self):
        """ RG真人大廳報表 - 當前 """

        result = self.live_ar.auto_report()
        self.assertEqual(True, result)

    def test_rg_live_lobby_report_last(self):
        """ RG真人大廳報表 - 歷史 """

        result = self.live_ar.auto_report(now=False)
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        LiveLobbyReport("test_login"),
        LiveLobbyReport("test_rg_live_lobby_report_now"),
        LiveLobbyReport("test_rg_live_lobby_report_last"),
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='RG真人遊戲大廳報表',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
