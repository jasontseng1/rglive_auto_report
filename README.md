# RG真人遊戲大廳報表測試

## 介紹

1. 這個存儲庫目前進行 真人-自動化報表測試，確保報表正常運作。
2. 當手動QA進行下注後，提供下注帳密，來進行自動化報表確認。

## 功能

- 針對 RG真人遊戲大廳 進行 當前或歷史(昨天) 自動報表測試。

## 先決條件

- Python 3.9.13 版本。

## 執行

1. 安裝所需的依賴：
    ```bash
    pip3 install -r requirements.txt
    ```

2. 執行測試
   使用以下命令運行自動報表測試：
    ```bash
    python3 main.py  # 執行報表自動對帳
    ```
