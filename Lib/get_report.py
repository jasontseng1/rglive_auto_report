# _*_ coding: UTF-8 _*_
import re
import time
import decimal
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains


class MemberReport:
    """ 真人遊戲大廳報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img
        self.ac = ActionChains(driver)

    def set_search_date(self, now=True):
        """
        設定搜尋日期
        參數:
            now (bool): 當天，預設為 True
        """

        if not now:
            tz_utc_4 = datetime.timezone(datetime.timedelta(hours=-4))  # 美東時區
            now_date = datetime.datetime.today().astimezone(tz_utc_4)  # 美東當前時間
            start_date = (now_date + datetime.timedelta(days=-1)).strftime('%d')  # 起始日
            end_date = now_date.strftime('%d')  # 結束日
            # 開始日ele，結束日ele
            start_ele, end_ele = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located(
                (By.XPATH, "//div[@class='ctrl']//div[@class='input']")))

            # 關閉用 ele
            _ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='box bet-record']/div")))

            # 設定起始日
            start_ele.click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, f"//div[@class='vc-h-full']/span[text()='{start_date}']"))).click()
            # 點其他地方關閉日歷
            _ele.click()

            # 設定結束日
            end_ele.click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, f"//div[@class='vc-h-full']/span[text()='{end_date}']"))).click()
            # 點其他地方關閉日歷
            _ele.click()

            # 點擊查詢按鈕
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='button search']"))).click()
            time.sleep(1)
        else:
            pass

    def close_report(self):
        """
        關閉報表
        """

        # 關閉報表
        close_report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='box bet-record']//img[@class='close']")))
        self.ac.move_to_element(close_report).click().perform()
        time.sleep(1)

    def search_report(self, search_now=True):
        """
        搜尋報表
        參數:
            search_now (bool): 搜尋當前，預設為 True

        Returns:
            (list|bool): 查詢報表成功，回傳注單報表，失敗or無注單則是 False
        """

        # 點開報表
        report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@aria-label='報表']")))
        self.ac.move_to_element(report).click().perform()
        time.sleep(1)

        # 設定搜尋日期
        self.set_search_date(search_now)

        # 判斷是否有資料
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//table[@id='vgt-table']//tbody//th")))
            time.sleep(1)
            report_data = self.format_report()
            return report_data

        except:
            self.get_img("Fail: 報表無資料")
            # 關閉報表
            self.close_report()
            return False

    def format_report(self):
        """
        整理注單報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗or異常則是 False
        """

        try:
            # init
            result_list = []
            bet_list = []
            field_dict = {}

            # 資料欄位
            th_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(
                (By.XPATH, "//table[@id='vgt-table']//thead//th")))

            for index, ele in enumerate(th_list):
                # 取特定欄位
                if ele.get_attribute('class') == 'line-numbers':
                    # 設定 id 欄位
                    field_dict['id'] = index
                elif ele.text in ['注單編號', '時間', '桌號', '下注金額', '派彩', '有效投注']:
                    # 報表欄位顯示差異，故不用+1
                    field_dict[ele.text] = index

            # 額外新增一個，存第二層報表用
            field_dict['second'] = 7

            # 取得頁數資訊
            ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='footer__navigation__page-info']")))
            start_page, page_text = ele.text.replace(' ', '').split('-')
            now_page, total_page = page_text.split('/')
            # 起始頁數
            start_page = int(start_page)
            # 當前頁數
            now_page = int(now_page)
            # 總頁數
            total_page = int(total_page)

            page_count = 0
            while page_count < total_page:
                tr_number = len(self.driver.find_elements(By.XPATH, "//tbody/tr[./td]"))
                for _ in range(1, tr_number + 1):
                    bet_data = {}
                    for i in field_dict:
                        # id要另外處理
                        if i == 'id':
                            ele = self.driver.find_element(By.XPATH, f"//tbody/tr[{_}]/th")
                            bet_data[i] = ele.text

                        elif i == 'second':
                            # 整理第二層報表
                            _second = self.driver.find_element(By.XPATH, f"//tbody/tr[{_}]/td[{field_dict[i]}]")
                            self.ac.move_to_element(_second).click().perform()
                            time.sleep(1)
                            # wait loading
                            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                                (By.XPATH, "//div[@class='bet-record-container']")))
                            time.sleep(1)
                            bet_data[i] = self.format_second_report()
                        else:
                            ele = self.driver.find_element(By.XPATH, f"//tbody/tr[{_}]/td[{field_dict[i]}]")
                            bet_data[i] = ele.text

                    bet_list.append(bet_data)

                # 換頁前檢查當頁小計
                subtotal_check = self.check_now_subtotal(bet_list, start_page, now_page)
                if not subtotal_check:
                    self.get_img('Fail: 當頁小計有誤')
                    result_list.append("False")

                if now_page != total_page:
                    count = 0
                    while count < 2:
                        # 換頁
                        _page = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//span[text()='下一頁']")))
                        self.ac.move_to_element(_page).click().perform()
                        time.sleep(1)
                        _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, "//div[@class='footer__navigation__page-info']")))
                        time.sleep(1)

                        # 新起始
                        new_start_page, page_text = _ele.text.replace(' ', '').split('-')
                        new_start_page = int(new_start_page)

                        # 新頁碼, 新總頁數
                        new_page, new_total_page = page_text.split('/')
                        new_page = int(new_page)
                        new_total_page = int(new_total_page)

                        # 判斷總頁數是否有變化
                        if total_page != new_total_page:
                            total_page = new_total_page
                            # 重新點擊搜尋
                            search = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                                (By.XPATH, "//img[@src='img/search-solid.9c4d71fa.svg']")))
                            self.ac.move_to_element(search).click().perform()
                            time.sleep(1)
                            # 重新設定
                            page_count = 0
                            count += 2

                        elif now_page == new_page:
                            count += 1
                            if count == 2:
                                self.get_img("Fail：注單換頁失敗")
                                page_count = total_page + 1
                        else:
                            now_page = new_page
                            start_page = new_start_page
                            count += 2
                            page_count += 10
                else:
                    page_count = total_page + 1

            return bet_list

        except Exception as e:
            self.get_img("Fail：整理報表資料異常")
            print(e)
            return False

    def format_second_report(self):
        """
        整理注單第二層報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗則是 False
        """

        try:
            # init
            second_bet_data = {}

            # 資料欄位
            second_field_dict = ['注單編號', '時間', '桌號', '下注金額', '派彩']
            for i in second_field_dict:
                if i == '注單編號':
                    _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@class='bet-record-container']//div[@class='title']")))
                    second_bet_data['注單編號'] = _ele.text[5:]
                else:
                    _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, f"//div[@class='detail']//div[text()='{i}']/following-sibling::div")))
                    second_bet_data[i] = _ele.text

            # 關閉第二層報表
            back_btn = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//img[@class='back']")))
            self.ac.move_to_element(back_btn).click().perform()
            time.sleep(1)

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='bet-record-container']")))
            time.sleep(1)

            return second_bet_data

        except Exception as e:
            self.get_img("Fail：整理第二層報表異常")
            print(e)
            return False

    def check_now_subtotal(self, report_data, start_number, end_number):
        """
        檢查當前小計
        參數:
            report_data (list): 報表注單資料
            start_number (int): 起始數字
            end_number (int): 結束數字
        Returns:
            (bool): 比對資料，成功為True，失敗則是False
        """

        # init
        result_list = []
        field_dict = {}
        subtotal_data = {}
        report_total_bet_amount = 0
        report_total_bonus = 0
        report_total_actual_bet_amount = 0

        # 統計比對數字
        for i in range(start_number, end_number + 1):
            for j in report_data:
                if j['id'] == str(i):
                    report_total_bet_amount += decimal.Decimal(j['下注金額'])
                    report_total_bonus += decimal.Decimal(j['派彩'])
                    report_total_actual_bet_amount += decimal.Decimal(j['有效投注'])

        # 計算後在轉float避免顯示有誤
        report_total_bet_amount = float(report_total_bet_amount)
        report_total_bonus = float(report_total_bonus)
        report_total_actual_bet_amount = float(report_total_actual_bet_amount)

        # 取得小計資料欄位
        th_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(
            (By.XPATH, "//table[@id='vgt-table']//thead//th")))

        for index, ele in enumerate(th_list):
            # 取特定欄位
            if ele.text in ['下注金額', '派彩', '有效投注']:
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

        # 取得資料
        for i in field_dict:
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, f"//tbody//tr[last()]/th[{field_dict[i]}]")))
            subtotal_data[i] = float(ele.text)

        print("==================== RG真人大廳報表-當頁小計 - 檢查開始 ====================\n")

        print("檢查項目：報表小計欄位「下注金額」與 報表當頁「下注金額」總計比對")
        # 比對下注金額
        if subtotal_data['下注金額'] != report_total_bet_amount:
            print("Fail：報表小計欄位「下注金額」與 報表當頁「下注金額」總計不同")
            print(f"報表小計「下注金額」欄位：{subtotal_data['下注金額']}")
            print(f"報表當頁「下注金額」總計：{report_total_bet_amount}\n")
            result_list.append("False")

        print("檢查項目：報表小計欄位「派彩」與 報表當頁「派彩」總計比對")
        # 比對派彩
        if subtotal_data['派彩'] != report_total_bonus:
            print("Fail：報表小計欄位「派彩」與 報表當頁「派彩」總計不同")
            print(f"報表小計「派彩」欄位：{subtotal_data['派彩']}")
            print(f"報表當頁「派彩」總計：{report_total_bonus}\n")
            result_list.append("False")

        print("檢查項目：報表小計欄位「有效投注」與 報表當頁「有效投注」總計比對")
        # 比對有效投注
        if subtotal_data['有效投注'] != report_total_actual_bet_amount:
            print("Fail：報表小計欄位「有效投注」與 報表當頁「有效投注」總計不同")
            print(f"報表小計「有效投注」欄位：{subtotal_data['有效投注']}")
            print(f"報表當頁「有效投注」總計：{report_total_actual_bet_amount}\n")
            result_list.append("False")

        print("*** 檢查結束 ***\n")
        self.get_img('當頁小計')
        print("==================== RG真人大廳報表-當頁小計 - 檢查結束 ====================\n")

        results = False if "False" in str(result_list) else True
        return results

    def check_report_data(self, bet_list):
        """
        確認報表資料
        參數:
            bet_list (list): 報表注單資料

        Returns:
            (bool): 回傳確認資料，異常則是False
        """

        try:
            if not bet_list:
                return False

            # init
            result_list = []
            total_data = {'總輸贏': 0, '總有效押分': 0, '總投注': 0, '總計': 0}

            # 統計比對數字
            report_total_number = len(bet_list)  # 報表總筆數
            report_total_bet_amount = 0  # 報表總下注金額
            report_total_bonus = 0  # 報表總派彩
            report_total_actual_bet_amount = 0  # 報表總有效投注

            for x in bet_list:
                report_total_bet_amount += decimal.Decimal(x['下注金額'])
                report_total_bonus += decimal.Decimal(x['派彩'])
                report_total_actual_bet_amount += decimal.Decimal(x['有效投注'])

            # 計算後在轉float避免顯示有誤
            report_total_bet_amount = float(report_total_bet_amount)
            report_total_bonus = float(report_total_bonus)
            report_total_actual_bet_amount = float(report_total_actual_bet_amount)

            # 取得統計欄位
            total_ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='totalList']")))
            total_list = total_ele.text.replace(',', '').split('\n')

            for i in total_list:
                for j in total_data:
                    if j in i:
                        if j == '總計':
                            total_data[j] = int(re.findall(r'-?\d+\.?\d*', i)[0])
                        else:
                            total_data[j] = float(re.findall(r'-?\d+\.?\d*', i)[0])

            print("==================== 報表 - 檢查開始 ====================\n")

            print("檢查項目：「總輸贏」欄位 與 報表「總派彩」比對")
            # 比對總輸贏
            if total_data['總輸贏'] != report_total_bonus:
                print("Fail：「總輸贏」欄位 與 報表「總派彩」不同")
                print(f"官網「總輸贏」欄位：{total_data['總輸贏']}")
                print(f"報表「總派彩」：{report_total_bonus}\n")
                result_list.append("False")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「總有效押分」欄位 與 報表「總有效投注」比對")
            # 比對總有效押分
            if total_data['總有效押分'] != report_total_actual_bet_amount:
                print("Fail：「總有效押分」欄位 與 報表「總有效投注」不同")
                print(f"「總有效押分」欄位：{total_data['總有效押分']}")
                print(f"報表「總有效投注」：{report_total_actual_bet_amount}\n")
                result_list.append("False")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「總投注」欄位 與 報表「總下注金額」比對")
            # 比對總投注
            if total_data['總投注'] != report_total_bet_amount:
                print("Fail：「總投注」欄位 與 報表「總下注金額」不同")
                print(f"「總投注」欄位：{total_data['總投注']}")
                print(f"報表「總下注金額」：{report_total_bet_amount}\n")
                result_list.append("False")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「總筆數」欄位 與 報表「總筆數」比對")
            # 比對總筆數
            if total_data['總計'] != report_total_number:
                print("Fail：「總筆數」欄位 與 報表「總筆數」不同")
                print(f"「總筆數」欄位：{total_data['總計']}")
                print(f"報表「總筆數」：{report_total_number}\n")
                result_list.append("False")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「會員報表第一層」與「會員報表第二層」比對")

            for bet in bet_list:
                # 比對注單編號
                if bet['注單編號'] != bet['second']['注單編號']:
                    print("Fail：第一層「注單編號」與 第二層不同")
                    print(f"第一層「注單編號」：{bet['注單編號']}")
                    print(f"第二層「注單編號」：{bet['second']['注單編號']}\n")
                    result_list.append("False")

                # 比對時間
                elif bet['時間'] != bet['second']['時間']:
                    print("Fail：第一層「時間」與 第二層不同")
                    print(f"第一層「時間」：{bet['時間']}")
                    print(f"第二層「時間」：{bet['second']['時間']}\n")
                    result_list.append("False")

                # 比對桌號
                elif bet['桌號'] != bet['second']['桌號']:
                    print("Fail：第一層「桌號」與 第二層不同")
                    print(f"第一層「桌號」：{bet['桌號']}")
                    print(f"第二層「桌號」：{bet['second']['桌號']}\n")
                    result_list.append("False")

                # 比對下注金額
                elif bet['下注金額'] != bet['second']['下注金額']:
                    print("Fail：第一層「下注金額」與 第二層不同")
                    print(f"第一層「下注金額」：{bet['下注金額']}")
                    print(f"第二層「下注金額」：{bet['second']['下注金額']}\n")
                    result_list.append("False")

                # 比對派彩金額
                elif bet['派彩'] != bet['second']['派彩']:
                    print("Fail：第一層「派彩」與 第二層不同")
                    print(f"第一層「派彩」：{bet['派彩']}")
                    print(f"第二層「派彩」：{bet['second']['派彩']}\n")
                    result_list.append("False")

            print("*** 檢查結束 ***\n")
            print("==================== 報表 - 檢查結束 ====================\n")

            # 關閉報表
            self.close_report()

            results = False if "False" in str(result_list) else True
            return results

        except Exception as e:
            self.get_img("Fail：確認報表資料異常")
            print(e)
            return False
